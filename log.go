/* really basic level logging for go */
package log

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	NOTSET   Level = 0
	TRACE    Level = 5
	DEBUG    Level = 10
	INFO     Level = 20
	WARNING  Level = 30
	ERROR    Level = 40
	CRITICAL Level = 50
)

var LevelNames map[Level]string = map[Level]string{
	NOTSET:   "NOTSET",
	TRACE:    "TRACE",
	DEBUG:    "DEBUG",
	INFO:     "INFO",
	WARNING:  "WARN",
	ERROR:    "ERR",
	CRITICAL: "FATAL",
}

var LevelNamesBytes map[Level][]byte = map[Level][]byte{
	NOTSET:   []byte("NOTSET"),
	TRACE:    []byte("DEBUG"),
	DEBUG:    []byte("DEBUG"),
	INFO:     []byte("INFO"),
	WARNING:  []byte("WARN"),
	ERROR:    []byte("ERR"),
	CRITICAL: []byte("FATAL"),
}

type Level int

type Formatter func(level Level, message string, when time.Time) string

type Log struct {
	level     Level
	out       io.Writer
	err       io.Writer
	outlock   *sync.Mutex
	errlock   *sync.Mutex
	formatter Formatter
}

// Create a new log with out and err writers
// - levels notset, debug, and info are written to `out`
// - levels warning, error, and critical are written to `err`
func New(out io.Writer, err io.Writer) *Log {
	return &Log{
		out:     out,
		err:     err,
		outlock: &sync.Mutex{},
		errlock: &sync.Mutex{},
	}
}

// Set the Output writer
func (self *Log) SetOutputWriter(out io.Writer) io.Writer {
    self.outlock.Lock()
    defer self.outlock.Unlock()
    prev := self.out
    self.out = out
    return prev
}

// Set the Error writer
func (self *Log) SetErrorWriter(err io.Writer) io.Writer {
    self.errlock.Lock()
    defer self.errlock.Unlock()
    prev := self.err
    self.err = err
    return prev
}

// Set a new logging level and return the previous level
func (self *Log) SetLevel(l Level) Level {
	prev := self.level
	self.level = l
	return prev
}

// Set the log formatter and return the previous one
func (self *Log) SetFormatter(f Formatter) Formatter {
	prev := self.formatter
	self.formatter = f
	return prev
}

// io.Writer interface that automatically detects the error level based on the message written
func (self *Log) Write(v []byte) (int, error) {
	const MAX_HEADER = 30 // only look at the first MAX_HEADER number of bytes

	level := NOTSET
	for l, n := range LevelNamesBytes {
		if bytes.Contains(v[:MAX_HEADER], n) {
			level = l
			break
		}
	}

	if level < self.level {
		return 0, nil
	}

	if level < WARNING {
		self.outlock.Lock()
		defer self.outlock.Unlock()
		return self.out.Write(v)
	} else {
		self.errlock.Lock()
		defer self.errlock.Unlock()
		return self.err.Write(v)
	}
}

// Log a message at the provided level
func (self *Log) Log(level Level, message string) {
	if level < self.level {
		return
	}

	var s string
	if self.formatter == nil {
		s = self.DefaultFormatter(level, message, time.Now())
	} else {
		s = self.formatter(level, message, time.Now())
	}

	if level < WARNING {
		self.outlock.Lock()
		defer self.outlock.Unlock()
		io.WriteString(self.out, s)
	} else {
		self.errlock.Lock()
		defer self.errlock.Unlock()
		io.WriteString(self.err, s)
	}
}

func (self *Log) Logf(level Level, f string, args ...interface{}) {
	m := fmt.Sprintf(f, args...)
	self.Log(level, m)
}

func (self *Log) Trace(message string) {
	self.Log(TRACE, message)
}

func (self *Log) Tracef(f string, args ...interface{}) {
	self.Logf(TRACE, f, args...)
}

func (self *Log) Debug(message string) {
	self.Log(DEBUG, message)
}

func (self *Log) Debugf(f string, args ...interface{}) {
	self.Logf(DEBUG, f, args...)
}

func (self *Log) Info(message string) {
	self.Log(INFO, message)
}

func (self *Log) Infof(f string, args ...interface{}) {
	self.Logf(INFO, f, args...)
}

func (self *Log) Warning(message string) {
	self.Log(WARNING, message)
}

func (self *Log) Warn(message string) {
    self.Log(WARNING, message)
}

func (self *Log) Warningf(f string, args ...interface{}) {
	self.Logf(WARNING, f, args...)
}

func (self *Log) Warnf(f string, args ...interface{}) {
	self.Logf(WARNING, f, args...)
}

func (self *Log) Error(message string) {
	self.Log(ERROR, message)
}

func (self *Log) Errorf(f string, args ...interface{}) {
	self.Logf(ERROR, f, args...)
}

func (self *Log) Critical(message string) {
	self.Log(CRITICAL, message)
}

func (self *Log) Criticalf(f string, args ...interface{}) {
	self.Logf(CRITICAL, f, args...)
}

// Default format function to return a log message formatted like:
// "[INFO] 2006/01/02 15:04:05 message here..."
func (self *Log) DefaultFormatter(level Level, message string, when time.Time) string {
	name, ok := LevelNames[level]
	if !ok {
		name = fmt.Sprintf("%d", level)
	}

	timestamp := when.Format("2006/01/02 15:04:05")

	return fmt.Sprintf("%s [%s] %s\n", timestamp, name, message)
}

////////////
// GLOBAL //
////////////

var Global *Log = New(os.Stdout, os.Stderr)

func SetFormatter(f Formatter) Formatter {
	return Global.SetFormatter(f)
}

func SetOutputWriter(out io.Writer) io.Writer {
	return Global.SetOutputWriter(out)
}

func SetErrorWriter(err io.Writer) io.Writer {
	return Global.SetErrorWriter(err)
}

func SetLevel(level Level) Level {
	return Global.SetLevel(level)
}

func Trace(message string) {
	Global.Log(TRACE, message)
}

func Tracef(f string, args ...interface{}) {
	Global.Logf(TRACE, f, args...)
}

func Debug(message string) {
	Global.Log(DEBUG, message)
}

func Debugf(f string, args ...interface{}) {
	Global.Logf(DEBUG, f, args...)
}

func Info(message string) {
	Global.Log(INFO, message)
}

func Infof(f string, args ...interface{}) {
	Global.Logf(INFO, f, args...)
}

func Warning(message string) {
	Global.Log(WARNING, message)
}

func Warningf(f string, args ...interface{}) {
	Global.Logf(WARNING, f, args...)
}

func Error(message string) {
	Global.Log(ERROR, message)
}

func Errorf(f string, args ...interface{}) {
	Global.Logf(ERROR, f, args...)
}

func Critical(message string) {
	Global.Log(CRITICAL, message)
}

func Criticalf(f string, args ...interface{}) {
	Global.Logf(CRITICAL, f, args...)
}

////////////////////
// Log Pkg Compat //
////////////////////

func Fatal(v ...interface{}) {
	Global.Log(CRITICAL, fmt.Sprint(v...))
	os.Exit(1)
}

func Panic(v ...interface{}) {
	s := fmt.Sprint(v...)
	Global.Log(CRITICAL, s)
	panic(s)
}

func Println(v ...interface{}) {
	Global.Log(INFO, strings.TrimSpace(fmt.Sprintln(v...)))
}
